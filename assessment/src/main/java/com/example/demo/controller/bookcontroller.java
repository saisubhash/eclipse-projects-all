package com.example.demo.controller;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.book;
import com.example.demo.service.bookservice;

@RestController
@RequestMapping("/go")
public class bookcontroller {
   
	@Autowired
	private bookservice b_s;
	
	@GetMapping(path="/get")
	public ResponseEntity<?> getallbooks(){
		List<book> bookslist = b_s.getallboks();
		if(bookslist.size()>0) {
			return new ResponseEntity<List<book>>(bookslist, HttpStatus.OK);
		}
		return new ResponseEntity<String>("List is empty",HttpStatus.OK);
	}
	
	@PostMapping(path="/post")
	public ResponseEntity<?> addbook(@RequestBody book bk){
		book b = b_s.addbook(bk);
		if(b!=null) {
			return new ResponseEntity<book>(b,HttpStatus.CREATED);
		}
		return new ResponseEntity<book>(b, HttpStatus.CONFLICT);
	}
	
	@DeleteMapping("/{bookid}")
	public ResponseEntity<?> deletebook(@PathVariable int bookid){
		book delete_book = b_s.deletebook(bookid);
		if(delete_book!=null) {
			return new ResponseEntity<book>(delete_book,HttpStatus.OK);
		}
		return new ResponseEntity<String>("book not deleted",HttpStatus.CONFLICT);
	}
	
	@PutMapping
	public ResponseEntity<?> updatebook(@RequestBody book bk){
		book update_book = b_s.updatebook(bk);
		if(update_book!=null) {
			return new ResponseEntity<book>(update_book, HttpStatus.OK);
		}
		return new ResponseEntity<String>("Books not updated", HttpStatus.CONFLICT);
	}
	
	@GetMapping("/{bookid}")
	public ResponseEntity<?> getBookbyid(@PathVariable int bookid){
		book bk = b_s.getbookbyid(bookid);
		if(bk!=null) {
			return new ResponseEntity<book>(bk, HttpStatus.OK);
		}
		return new ResponseEntity<String>("Book not found", HttpStatus.OK);
	}
	
}
