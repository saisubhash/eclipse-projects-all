package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="books")
public class book {
  
	@Id
	@Column(length=45)
	private int bookid;
	@Column(length=45)
	private String booktitle;
	@Column(length=45)
	private String author;
	@Column(length=45)
	private double price;
	
	public book(){
		super();
	}
	
	

	public book(int bookid, String booktitle, String author, double price) {
		super();
		this.bookid = bookid;
		this.booktitle = booktitle;
		this.author = author;
		this.price = price;
	}



	public int getBookid() {
		return bookid;
	}

	public void setBookid(int bookid) {
		this.bookid = bookid;
	}

	public String getBooktitle() {
		return booktitle;
	}

	public void setBooktitle(String booktitle) {
		this.booktitle = booktitle;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}



	@Override
	public String toString() {
		return "book [bookid=" + bookid + ", booktitle=" + booktitle + ", author=" + author + ", price=" + price + "]";
	}
	
	
	
}
