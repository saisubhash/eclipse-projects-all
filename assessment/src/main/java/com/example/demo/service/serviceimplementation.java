package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.book;
import com.example.demo.repository.bookrepository;

import com.example.demo.model.book;

@Service
public class serviceimplementation implements bookservice{

	@Autowired
	private bookrepository book_repository;
	
	@Override
	public book addbook(book book_) {
		// TODO Auto-generated method stub
		book b = book_repository.save(book_);
		return b;
	}

	@Override
	public List<book> getallboks() {
		// TODO Auto-generated method stub
		return book_repository.findAll();
	}

	@Override
	public book updatebook(book book_) {
		// TODO Auto-generated method stub
		book b = book_repository.save(book_);
		return b;
	}

	@Override
	public book deletebook(int bookid){
		// TODO Auto-generated method stub
		String s=String.valueOf(bookid);  
		if(book_repository.existsById(s)) {
			book b = book_repository.findById(s).get();
			book_repository.deleteById(s);
			return b;
		}
		return null;
	}

	@Override
	public book getbookbyid(int bookid) {
		// TODO Auto-generated method stub
		String s=String.valueOf(bookid);  
		Optional<book> bookdata = book_repository.findById(s);
		if(bookdata.isPresent()) {
			return bookdata.get();
		}else {
			return null;
		}
	}
	
	public List<book> getallbooks(){
		return book_repository.findAll();
	}

}
