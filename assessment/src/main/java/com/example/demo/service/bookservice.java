package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.example.demo.model.book;

public interface bookservice {
   public book addbook(book book_);
   public List<book> getallboks();
   public book updatebook(book book_);
   public book deletebook(int book_id);
   public book getbookbyid(int book_id);
}
