package com.example.albumapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.albumapp.beanmodel.album;
import com.example.albumapp.service.albumservice;

@RestController
@RequestMapping("/api/v1/album")
public class AlbumController {
  @Autowired
  private albumservice albumservice;
  
  @PostMapping
  public ResponseEntity<?> addAlbum(@RequestBody album album){
	  System.out.println(album);
	  album alb = albumservice.addalbum(album);
	  if(alb!=null) {
		  return new ResponseEntity<album>(alb,HttpStatus.CREATED);
	  }else {
		  return new ResponseEntity<String>("Album not created", HttpStatus.CONFLICT);
	  }
  }
    @GetMapping
    public ResponseEntity<?> getAllAlbums(){
    	List<album> albumlist = albumservice.getallalbums();
    	if(albumlist.size()>0) {
    		return new ResponseEntity<List<album>>(albumlist,HttpStatus.OK);
    	}
    	return new ResponseEntity<String>("List is Empty",HttpStatus.CONFLICT);
    }
    
    public ResponseEntity<?> updateAlbum(@RequestBody album album){
    	album updatealb = albumservice.updatealbum(album);
    	if(updatealb!=null) {
    		return new ResponseEntity<album>(updatealb,HttpStatus.OK);
    	}
    	return new ResponseEntity<String>("Album not updaed",HttpStatus.OK);
    }
    
    @DeleteMapping("/{albumid}")
    public ResponseEntity<?> deleteAlbum(@PathVariable String albumid){
    	album deletealb = albumservice.deletealbum(albumid);
    	if(deletealb!=null) {
    		return new ResponseEntity<album>(deletealb,HttpStatus.OK);
    	}
    	return new ResponseEntity<String>("Album not deleted",HttpStatus.OK);
    }
    
    @GetMapping("/{albumid}")
    public ResponseEntity<?> getAlbumById(@PathVariable String albumid){
    	album alb = albumservice.getalbumbyid(albumid);
    	if(alb!=null) {
    		return new ResponseEntity<album>(alb,HttpStatus.OK);
    	}
    	return new ResponseEntity<String>("Album not found", HttpStatus.CONFLICT);
    }
}
