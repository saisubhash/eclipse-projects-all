package com.example.albumapp.beanmodel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class album {
	
  @Id
  @Column(length=45)
  private String albumid;
  @Column(length=45)
  private String albumname;
  @Column(length=45)
  private String singer;
  @Column(length=20)
  private double price;
  @Column(length=45)
  private String language;
  
  public album() {
	  super();
  }
  
  public album(String albumid, String albumname, String singer, double price, String language) {
	  super();
	  this.albumid = albumid;
	  this.albumname = albumname;
	  this.singer = singer;
	  this.price = price;
	  this.language = language;
  }
  
 public String getAlbumname() {
	return albumname;
}
public void setAlbumname(String albumname) {
	this.albumname = albumname;
}
public String getAlbumid() {
	return albumid;
}
public void setAlbumid(String albumid) {
	this.albumid = albumid;
}
public String getSinger() {
	return singer;
}
public void setSinger(String singer) {
	this.singer = singer;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public String getLanguage() {
	return language;
}
public void setLanguage(String language) {
	this.language = language;
}
  
}
