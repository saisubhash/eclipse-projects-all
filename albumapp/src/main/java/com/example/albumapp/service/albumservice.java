package com.example.albumapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.albumapp.repository.*;
import com.example.albumapp.beanmodel.album;

@Service
public class albumservice implements albuminterface{
    @Autowired
	private albumrepository albumrepository;
	
	@Override
	public album addalbum(album alb) {
		album album1 = albumrepository.save(alb);
		// TODO Auto-generated method stub
		return album1;
	}

	@Override
	public List<album> getallalbums() {
		// TODO Auto-generated method stub
		return albumrepository.findAll();
	}

	@Override
	public album updatealbum(album alb) {
		// TODO Auto-generated method stub
		if(albumrepository.existsById(alb.getAlbumid())) {
			return null;
		}
		albumrepository.save(alb);
		return null;
	}

	@Override
	public album deletealbum(String albumid) {
		// TODO Auto-generated method stub
		if(albumrepository.existsById(albumid)) {
			album alb = albumrepository.findById(albumid).get();
			albumrepository.deleteById(albumid);
			return alb;
		}
		return null;
	}

	@Override
	public album getalbumbyid(String albumid) {
		// TODO Auto-generated method stub
		Optional<album> albumdata = albumrepository.findById(albumid);
		if(albumdata.isPresent()) {
			return albumdata.get();
		}else {
			return null;
		}
	}

	@Override
	public List<album> getallalbumsbysinger(String singername) {
		// TODO Auto-generated method stub
		return albumrepository.findAll();
	}
  
	
}
