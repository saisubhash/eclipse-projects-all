package com.example.albumapp.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.albumapp.beanmodel.album;

public interface albuminterface {
    public album addalbum(album alb);
    public List<album> getallalbums();
    public album updatealbum(album alb);
    public album deletealbum(String albumid);
    public album getalbumbyid(String albumid);
    public List<album> getallalbumsbysinger(String singername);
}
