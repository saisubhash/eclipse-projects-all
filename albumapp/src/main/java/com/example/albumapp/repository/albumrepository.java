package com.example.albumapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.albumapp.beanmodel.album;

@Repository
public interface albumrepository extends JpaRepository<album, String>{
     
}
