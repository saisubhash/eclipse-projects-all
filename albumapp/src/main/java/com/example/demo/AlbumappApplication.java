package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlbumappApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlbumappApplication.class, args);
		System.out.println("running");
	}

}
