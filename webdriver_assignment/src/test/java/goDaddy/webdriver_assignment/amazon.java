package goDaddy.webdriver_assignment;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

@Test
public class amazon {
  WebDriver driver;
  
  @Test(priority = 1)
  public void test1() throws InterruptedException {
	  String pagetitle=driver.getTitle();
	  System.out.println(pagetitle);
	  Actions a=new Actions(driver);
	  WebElement e=driver.findElement(By.linkText("Amazon miniTV"));
	  a.moveToElement(e).perform();
	  Thread.sleep(2000);
	  e.click();
	  System.out.println(driver.getTitle());
	  Assert.assertEquals("Watch Free Web Series & Short Films Online | Amazon miniTV",driver.getTitle());
	  Thread.sleep(1000);
	  driver.navigate().back();
	  Assert.assertEquals("Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in",pagetitle);
	  Thread.sleep(2000);
  }
  
  @Test(priority = 2)
  public void test2() throws InterruptedException {
	  String pagetitle=driver.getTitle();
	  Actions a=new Actions(driver);
	  WebElement e=driver.findElement(By.linkText("Sell"));
	  a.moveToElement(e).perform();
	  Thread.sleep(2000);
	  e.click();
	  System.out.println(driver.getTitle());
	  Assert.assertEquals("Amazon.in: CrossShop1",driver.getTitle());
	  Thread.sleep(1000);
	  driver.navigate().back();
	  Assert.assertEquals("Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in",pagetitle);
	  Thread.sleep(2000);
  }
  @Test(priority = 3)
  public void test3() throws InterruptedException {
	  String pagetitle=driver.getTitle();
	  Actions a=new Actions(driver);
	  WebElement e=driver.findElement(By.linkText("Best Sellers"));
	  a.moveToElement(e).perform();
	  Thread.sleep(2000);
	  e.click();
	  System.out.println(driver.getTitle());
	  Assert.assertEquals("Amazon.in Bestsellers: The most popular items on Amazon",driver.getTitle());
	  Thread.sleep(1000);
	  driver.navigate().back();
	  Assert.assertEquals("Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in",pagetitle);
	  Thread.sleep(2000);
  }
  
  @Test(priority = 4)
  public void test4() throws InterruptedException {
	  String pagetitle=driver.getTitle();
	  Actions a=new Actions(driver);
	  WebElement e=driver.findElement(By.linkText("Today's Deals"));
	  a.moveToElement(e).perform();
	  Thread.sleep(2000);
	  e.click();
	  System.out.println(driver.getTitle());
	  Assert.assertEquals("Amazon.in - Today's Deals",driver.getTitle());
	  Thread.sleep(1000);
	  driver.navigate().back();
	  Assert.assertEquals("Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in",pagetitle);
	  Thread.sleep(2000);
  }
  
  @BeforeTest
  public void beforeTest() {
	  WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.get("https://www.amazon.in");
		driver.manage().window().maximize();
  }
  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}