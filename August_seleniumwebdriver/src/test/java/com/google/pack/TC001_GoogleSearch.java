package com.google.pack;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

//import com.sun.tools.javac.util.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TC001_GoogleSearch {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
     	driver.get("https://www.godaddy.com/");
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		WebElement ele = driver.findElement(By.name("q"));
		ele.sendKeys("Hello narayana");
	 // System.out.println(driver.getPageSource());
		System.out.println("The title is: "+driver.getTitle());
        driver.navigate().to("https://www.yahoo.com");
        System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getPageSource());
		driver.navigate().refresh();
		Thread.sleep(3000);
        System.out.println("The url is: "+driver.getCurrentUrl());
         
        driver.navigate().to("https://demo.opencart.com/");
	}

}
