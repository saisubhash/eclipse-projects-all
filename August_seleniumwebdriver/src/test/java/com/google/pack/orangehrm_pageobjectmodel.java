package com.google.pack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class orangehrm_pageobjectmodel {
  WebDriver driver;
  By ohrmusername = By.name("username");
  By ohrmpassword = By.name("password");
  By ohrmsubmit = By.xpath("//button[@type='submit']");
  By ohrmsearch = By.xpath("//input[@placeholder='Search']");
  
  public orangehrm_pageobjectmodel(WebDriver driver2){
	  driver = driver2;
  }
  
  public void enterusername(String uname) {
	  driver.findElement(ohrmusername).sendKeys(uname);
  } 
  
  public void enterpassword(String pword) {
	  driver.findElement(ohrmpassword).sendKeys(pword);
  }
  
  public void clicksubmit() {
	  driver.findElement(ohrmpassword).click();
  }
  
  public boolean searchdisplay() {
	  Boolean search = driver.findElement(ohrmpassword).isDisplayed();
	  return search;
  }
  
} 
