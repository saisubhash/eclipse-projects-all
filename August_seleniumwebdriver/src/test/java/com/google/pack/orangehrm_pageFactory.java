 package com.google.pack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class orangehrm_pageFactory {
  
  @FindBy(name="username")
  WebElement ohrmusername;
  
  @FindBy(name="password")
  WebElement ohrmpassword;
  
  @FindBy(xpath="//button[@type='submit']")
  WebElement ohrmsubmit;
  
  @FindBy(xpath="//input[@placeholder='Search']")
  WebElement ohrmsearch;
  
  
  public void enterusername(String uname) {
	   ohrmusername.sendKeys(uname);
  } 
  
  public void enterpassword(String pword) {
	 ohrmpassword.sendKeys(pword);
  }
  
  public void clicksubmit() {
	  ohrmsubmit.click();
  }
  
  public boolean searchdisplay() {
	   return ohrmsearch.isDisplayed();
  }
  
} 
