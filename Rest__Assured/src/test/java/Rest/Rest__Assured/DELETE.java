package Rest.Rest__Assured;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.*;

public class DELETE {
  @Test
  public void del() {
	  baseURI = "https://reqres.in";
	  when()
	  .delete("/api/users/2").then().statusCode(204).log().all();
  }
}