package Rest.Rest__Assured;

import org.testng.annotations.Test;
import static org.hamcrest.CoreMatchers.equalTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

public class PATCH {
  @Test
  public void patch() {
	  JSONObject req=new JSONObject();
	  req.put("name", "Kavya");
	  req.put("job", "Engineer");
	  baseURI="https://reqres.in/";
	  
	  given()
	  .header("Content-Type","application/json")
	  .contentType(ContentType.JSON).
	  accept(ContentType.JSON)
	  .body(req.toJSONString())
	  .when()
	  .patch("/api/users/2")
	  .then()
	  .statusCode(200).body("name", equalTo("Kavya"))
	  .log().all();
	    
  }
}
