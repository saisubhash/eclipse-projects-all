package Rest.Rest__Assured;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static io.restassured.RestAssured.*;

public class GET_LISTUSER_BDD {
  
@Test
  public void getlistbdd() {
	  baseURI= "https://reqres.in/api";
	  
	  given().
	  get("/users?page=2").
	  then().
	  statusCode(200).body("data[0].first_name",equalTo("Michael")).log().all();
  }
}