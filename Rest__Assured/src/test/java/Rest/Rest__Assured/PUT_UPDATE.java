package Rest.Rest__Assured;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.json.simple.JSONObject;
import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;

public class PUT_UPDATE {
  @Test
  public void put() {
	  
	  JSONObject req=new JSONObject();
	  req.put("name", "human");
	  req.put("job", "engineer");
	  baseURI="https://reqres.in/api";
	  
	  given()
	  .header("Content-Type","application/json")
	  .contentType(ContentType.JSON).
	  accept(ContentType.JSON)
	  .body(req.toJSONString())
	  .when()
	  .put("/users/2")
	  .then()
	  .statusCode(200).body("name", equalTo("human"))
	  .log().all();
	    }
}