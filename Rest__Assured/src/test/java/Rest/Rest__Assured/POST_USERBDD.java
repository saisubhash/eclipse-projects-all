package Rest.Rest__Assured;

import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.*;

public class POST_USERBDD {
  @Test
  public void postuser() {
	  JSONObject req=new JSONObject();
	  req.put("name", "Deepika");
	  req.put("job", "developer");
	  baseURI="https://reqres.in/api";
	  given().body(req.toJSONString()).when().post("/users").then().statusCode(201).log().all();
  }
}