package Amazon.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class pom {
	WebDriver driver1;
	
   	 By search = By.xpath("//input[@placeholder=\"Search Amazon.in\"]");
	  By searchButton = By.xpath("//input[@id=\"nav-search-submit-button\"]");
	  By Product = By.xpath("//div[@data-index=\"4\"]");
	  By ProductName = By.xpath("//span[@id=\"productTitle\"]");
	  By addToCart = By.xpath("//input[@id=\"add-to-cart-button\"]");
	  By goToCart = By.xpath("//input[@aria-labelledby=\"attach-sidesheet-view-cart-button-announce\"]"); 
	  public pom(WebDriver driver) {
		  	 this.driver1 = driver;
		  }

		  public void searchProduct(String productName) {
		  	driver1.findElement(search).sendKeys(productName);
		      driver1.findElement(searchButton).click();
		  }
		  
		  public void clickOnProduct() {
		  	 driver1.findElement(Product).click();
		  }
		  
		  public void Cart() {
		  	driver1.findElement(addToCart).click();
		  	driver1.findElement(goToCart).click();
		  }
		  
		  By productAdded = By.linkText(driver1.findElement(ProductName).getText());
		  
		  public Boolean isAdded() {
		  	
		  	Boolean isProductAdded = driver1.findElement(productAdded).isDisplayed();
		  	return isProductAdded;
		  }
}
