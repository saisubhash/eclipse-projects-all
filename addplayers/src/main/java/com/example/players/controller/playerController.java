package com.example.players.controller;


import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.players.model.Player;
import com.example.players.service.playerService;
import com.example.players.service.playerServiceImplementation;

@RestController
@RequestMapping("/go/v1")
public class playerController {
//    @RequestMapping(path="/z", method=RequestMethod.GET)
//	public String ex() {
//		return "Working fine";
//	}
	
	@Autowired
	private playerService player_Service;
	
	@GetMapping
	public ResponseEntity<?> getAllPlayers(){
		List<Player> playerslist = player_Service.getAllPlayers();
		if(playerslist.size()>0) {
			return new ResponseEntity<List<Player>>(playerslist,HttpStatus.OK);
		}
		return new ResponseEntity<String>("List is empty",HttpStatus.OK);
	}
	
	@PostMapping(path="/post")
	public ResponseEntity<?> addPlayer(@RequestBody Player player){
		System.out.println(player);
		Player plr = player_Service.addPlayer(player);
		if(plr!=null) {
			return new ResponseEntity<Player>(plr,HttpStatus.CREATED);
		}
		return new ResponseEntity<Player>(plr,HttpStatus.CONFLICT);
	}
}
