package Restassuredassignment.AssuredAssignment;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;


public class POSTPRODUCTS {
  @Test
  public void postproduc() {
	  JSONObject req = new JSONObject();
	 // req.put("productId", 19);
	  req.put("productName", "king");
	  req.put("productDescription", "tasty");
	  req.put("productPrice", 45.0);
	  baseURI = "https://localhost:9101/productservice";
	  given().body(req.toJSONString()).when().post("/addProduct").then().statusCode(201).log().all();
  }
}
