package Restassured__assignment.Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class POST {
  @Test
  public void f() {
	  JSONObject req = new JSONObject();
	  req.put("productId", 19);
	  req.put("productName", "king");
	  req.put("productDescription", "tasty");
	  req.put("productPrice", 45.0);
	  baseURI = "https://localhost:9101/productservice";
	  given().body(req.toJSONString()).when().post("/addProduct").then().statusCode(201).log().all();
  }
}
