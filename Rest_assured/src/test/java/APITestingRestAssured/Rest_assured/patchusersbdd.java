package APITestingRestAssured.Rest_assured;

import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.json.simple.JSONObject;

public class patchusersbdd {
  @Test
  public void patchuser() {
	  JSONObject req = new JSONObject();
	  req.put("name", "kavya");
	  req.put("job", "Engineer");
	  baseURI = "https://reqres.in";
      given().header("Content-Type","appplication/json").contentType(ContentType.JSON).accept(ContentType.JSON)
      .body(req.toJSONString()).when().patch("/api/users/2").then().statusCode(200).body("name", equalTo("kavya")).log().all();
  }
}
