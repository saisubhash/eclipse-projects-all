package APITestingRestAssured.Rest_assured;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;

public class postusersbdd {
  @Test
  public void postuser() {
	  JSONObject req = new JSONObject();
	  req.put("name", "Deepika"); 
	  req.put("job", "developer");
	  baseURI = "https://reqres.in/api";
	 given().body(req.toJSONString()).when().post("/users").then().statusCode(201).log().all();	  
  }
}
