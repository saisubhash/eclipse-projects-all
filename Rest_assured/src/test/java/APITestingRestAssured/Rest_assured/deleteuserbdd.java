package APITestingRestAssured.Rest_assured;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.when;


public class deleteuserbdd {
  @Test
  public void deleteuser() {
	  baseURI = "https://reqres.in";
	  when().delete("/api/users/2").then().statusCode(204).log().all();
	  
  }
}
