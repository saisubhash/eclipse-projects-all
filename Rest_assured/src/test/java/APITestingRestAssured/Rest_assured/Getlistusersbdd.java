package APITestingRestAssured.Rest_assured;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class Getlistusersbdd {
  @Test
  public void getusers() {
	  baseURI = "https://reqres.in/api";
		//  System.out.println(given().get("/users?page=2").getBody().asString());
		  given().get("/users?page=2").then().
		  statusCode(200).body("data[0].first_name", equalTo("Michael")).log().all();
  }
}
