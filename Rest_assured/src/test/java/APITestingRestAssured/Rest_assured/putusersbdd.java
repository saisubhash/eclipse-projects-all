package APITestingRestAssured.Rest_assured;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.json.simple.JSONObject;

import io.restassured.http.ContentType;

public class putusersbdd {
  @Test
  public void putusers() {
	  JSONObject req = new JSONObject();
	  req.put("name", "kavya");
	  req.put("job","Engineer");
	  baseURI = "https://reqres.in/api";
	  
	  given()
	  .header("Content-Type","application/json")
	  .contentType(ContentType.JSON).accept(ContentType.JSON)
	  .body(req.toJSONString())
	  .when()
	  .put("users/2")
	  .then().statusCode(200).body("name", equalTo("kavya")).log().all();
  }
}
