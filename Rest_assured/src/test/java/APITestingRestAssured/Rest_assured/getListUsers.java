package APITestingRestAssured.Rest_assured;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class getListUsers {
  @Test
  public void getListUsers() {
	  Response res = RestAssured.get("https://reqres.in/api/users?page=2");
	  Response res_1 = RestAssured.get("https://reqres.in/api/unknown/2");
	  System.out.println("Status COde "+res.getStatusCode());
	  System.out.println("Response Time:"+res.getTime());
	  System.out.println("Response Body: "+res.getBody().asString());
	  System.out.println("Response body for list users"+res_1.getBody().asString());
	  System.out.println("Status Line "+res.getStatusLine());
	  System.out.println("Header "+res.getHeader("Content-type"));
  }
}
