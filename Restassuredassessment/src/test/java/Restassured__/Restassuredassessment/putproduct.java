package Restassured__.Restassuredassessment;

import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.baseURI;

public class putproduct {
  @Test
  public void f() {
	  JSONObject obj = new JSONObject();
	  baseURI = "http://localhost:9101/productservice";
	  obj.put("productId", "58");
	  obj.put("productName", "Orange");
	  obj.put("productPrice", 14);
	  
	  given().contentType("application/json").when().body(obj.toJSONString())
	  .put("/58")
	  .then()
	  .log().all();
  }
}
