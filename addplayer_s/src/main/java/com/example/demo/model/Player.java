package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name="Player")
public class Player {
    
   @Id
   @Column(length=45)
   private String playerId;
   @Column(length=45)
   private String playerName;
   @Column(length=45)
   private String sports;
   @Column(length=45)
   private String country;
   
   public Player(){
	   super();
   }
   
   public Player(String playerId, String playerName, String sports, String country) {
	   super();
	   this.playerId = playerId;
	   this.playerName = playerName;
	   this.sports = sports;
	   this.country = country;
   }
   
   public String getPlayerId() {
	return playerId;
   }
   public void setPlayerId(String playerId) {
	this.playerId = playerId;
}
public String getPlayerName() {
	return playerName;
}
public void setPlayerName(String playerName) {
	this.playerName = playerName;
}
public String getSports() {
	return sports;
}
public void setSports(String sports) {
	this.sports = sports;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}

   @Override
   public String toString() {
	   return "Players [playerId="+playerId+", playerName="+playerName+" sports="+sports+" country="+country;   
   }
}
