package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Player;

public interface playerService {
    public Player addPlayer(Player player);
    public List<Player> getAllPlayers();
    public Player updatePlayer(Player player);
    public Player deletePlayer(String playerid);
    public Player getPlayerbyId(String playerid);
}

