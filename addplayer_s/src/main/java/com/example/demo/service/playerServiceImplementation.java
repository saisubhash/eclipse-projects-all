package com.example.demo.service;





import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Player;


@Service
public class playerServiceImplementation implements playerService{
	
	@Autowired
	private playerrepository playerrepository;
	
	
	public Player addPlayer(Player player) {
		// TODO Auto-generated method stub
		Player pl = playerrepository.save(player);
		return pl;
	}

	
	public List<Player> getAllPlayers() {
		// TODO Auto-generated method stub
		return playerrepository.findAll();
	}


	@Override
	public Player updatePlayer(Player player) {
		// TODO Auto-generated method stub
		playerrepository.save(player);
		return player;
	}


	@Override
	public Player deletePlayer(String playerid) {
		// TODO Auto-generated method stub
		 if(playerrepository.existsById(playerid)) {
			 Player player = playerrepository.findById(playerid).get();
			 playerrepository.deleteById(playerid);
			 return player;
		 }
		 return null;
	}


	@Override
	public Player getPlayerbyId(String playerid) {
		// TODO Auto-generated method stub
		Optional<Player> playerdata = playerrepository.findById(playerid);
		if(playerdata.isPresent()) {
			return playerdata.get();
		}
		return null;
	}
}
