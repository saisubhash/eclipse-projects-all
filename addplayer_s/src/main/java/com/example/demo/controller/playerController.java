package com.example.demo.controller;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Player;
import com.example.demo.service.playerService;
//import com.stackroute.albumapp.model.Album;



@RestController
@RequestMapping("/go/v1")
public class playerController {
	
	@Autowired
	private playerService player_Service;
	
	@GetMapping(path="/get")
	public ResponseEntity<?> getAllPlayers(){
		List<Player> playerslist = player_Service.getAllPlayers();
		if(playerslist.size()>0) {
			return new ResponseEntity<List<Player>>(playerslist,HttpStatus.OK);
		}
		return new ResponseEntity<String>("List is empty",HttpStatus.OK);
	}
	
	@PostMapping(path="/post")
	public ResponseEntity<?> addPlayer(@RequestBody Player player){
		System.out.println(player);
		Player plr = player_Service.addPlayer(player);
		if(plr!=null) {
			return new ResponseEntity<Player>(plr,HttpStatus.CREATED);
		}
		return new ResponseEntity<Player>(plr,HttpStatus.CONFLICT);
	}
	
	@PutMapping
	public ResponseEntity<?> updatePlayer(@RequestBody Player player){
		Player plr = player_Service.updatePlayer(player);
		if(plr!=null) {
			return new ResponseEntity<Player>(plr, HttpStatus.OK);
		}
		return new ResponseEntity<String>("Players not updated", HttpStatus.CONFLICT);
	}
	
	@DeleteMapping("/{playerid}")
	public ResponseEntity<?> deleteAlbum(@PathVariable String playerid) {
		
		Player deleteplr = player_Service.deletePlayer(playerid);;
		if( deleteplr !=null) {
			return new ResponseEntity<Player>(deleteplr,HttpStatus.OK);
		}
		
		return new ResponseEntity<String>("Player not Deleted",HttpStatus.CONFLICT);
	}
	
	@GetMapping("/{playerId}")
	public ResponseEntity<?> getPlayerbyId(@PathVariable String playerId){
		Player player = player_Service.getPlayerbyId(playerId);
		if(player!=null) {
			return new ResponseEntity<Player>(player,HttpStatus.OK);
		}
		return new ResponseEntity<String>("Player not found",HttpStatus.OK);
	}
	
}

