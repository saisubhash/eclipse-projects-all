Feature: feature to test Amazon Website

  Scenario: checking login procedure
    Given launch the browser
    And Navigate to the URL
    And User selects the login option
    And the user enters the "<email>"
    And User clicks continue
    And the user enters the "<password>" and login
    #Then User closes the Browser
    
     Examples: 
    | email                | password   |
    | rp5.thegoalpost@gmail.com | password |
    
    
    
  Scenario: adding product to cart
    And the user enters a "<productname>"
    And the user clicks on Search Button
    And User Selects the First product
    And User adds product to cart
    And User navigates to another window
    And User goes to cart
    Then User closes the Browser
    
    Examples: 
    | productname  |
    | I phone 13 |
 
  