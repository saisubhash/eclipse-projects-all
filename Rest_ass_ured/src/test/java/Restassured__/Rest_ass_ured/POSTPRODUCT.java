package Restassured__.Rest_ass_ured;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import org.json.simple.JSONObject;
import static io.restassured.RestAssured.*;

public class POSTPRODUCT {
  @Test
  public void post_users() {
	  JSONObject req = new JSONObject();
	  req.put("productId", 58);
	  req.put("productName", "Cosmetic");
	  req.put("productDescription", "Beaut");
	  req.put("productPrice", 340);
	  baseURI = "http://localhost:9102/productservice";
	  given().body(req.toJSONString()).header("Content-Type","application/json")
	  .contentType(ContentType.JSON).
	  when().post("/addProduct").then().statusCode(201).log().all();
  }
}
