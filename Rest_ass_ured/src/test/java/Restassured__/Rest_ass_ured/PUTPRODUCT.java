package Restassured__.Rest_ass_ured;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static org.hamcrest.CoreMatchers.equalTo;

import org.json.simple.JSONObject;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.baseURI;

public class PUTPRODUCT {
  @Test
  public void put_users() {
	  JSONObject req=new JSONObject();
	  baseURI="http://localhost:9102/productservice";
	  
	  req.put("productId",2);
	  req.put("productName", "Pomogranete");
	  req.put("productPrice", 14);
	  
	  given()
	  .contentType("application/json")
	  .when()
	  .body(req.toJSONString())
	  .put("/2")
	  .then()
	  .log().all();
  }
}
