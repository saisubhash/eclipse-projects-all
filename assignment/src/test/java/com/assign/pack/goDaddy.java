package com.assign.pack;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class goDaddy {
  @Test
  public void f() {
	  WebDriverManager.chromedriver().setup();
	  WebDriver driver = new ChromeDriver();
	  driver.get("https://www.godaddy.com/");
	  driver.manage().window().maximize();
	  System.out.println(driver.getTitle());
	  String acttitle = driver.getTitle();
	  Assert.assertEquals(acttitle,"Domain Names, Websites, Hosting & Online Marketing Tools - GoDaddy IN");
	  String url = driver.getCurrentUrl();
	  Assert.assertEquals(url, "https://www.godaddy.com/en-in");
	  
  }
}
